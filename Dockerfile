FROM mysql:8.0
COPY create-tables.sql /docker-entrypoint-initdb.d/
EXPOSE 3306
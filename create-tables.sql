CREATE DATABASE task_tracker;
USE task_tracker;

CREATE TABLE users (
  `user_id` int AUTO_INCREMENT NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password_hash` varchar(255) NOT NULL,
  `salt` varchar(255) NOT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY (`email`)
);

CREATE TABLE categories (
  `category_id` int AUTO_INCREMENT NOT NULL,
  `owner` int NOT NULL,
  `name` varchar(255) NOT NULL,
  `active` boolean NOT NULL DEFAULT TRUE,
  PRIMARY KEY (`category_id`),
  FOREIGN KEY (`owner`) REFERENCES users(`user_id`) ON DELETE CASCADE
);

CREATE TABLE tasks (
  `task_id` int AUTO_INCREMENT NOT NULL,
  `owner` int NOT NULL,
  `status` int NOT NULL,
  `date` date NOT NULL,
  `text` varchar(255) NOT NULL,
  `category_id` int,
  PRIMARY KEY (`task_id`),
  FOREIGN KEY (`owner`) REFERENCES users(`user_id`) ON DELETE CASCADE,
  FOREIGN KEY (`category_id`) REFERENCES categories(`category_id`) ON DELETE SET NULL
);